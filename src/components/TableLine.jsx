import React from 'react';

const TableLine = ({coin, index}) => {

  const priceFormater = (nb) => {
    if(Math.round(nb).toString().length < 4) {
      return new Intl.NumberFormat("us-US", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 7
      }).format(nb)
    }
    return nb
  }

  const mktcapFormater = (nb) => {
    return Number(String(nb).split('').slice(0, -6).join(''))
  }

  return (
    <div className="table-line">
      <div className="infos-container">
        <span>*</span>
        <p>{index + 1}</p>
        <div className="img"><img src={coin.image} alt="logo" height="20" /></div>
        <div className="infos">
          <div className="chart-img">
            <img src="assets/chart-icon.svg" alt="chart" />
          </div>
          <h4>{coin.name}</h4>
          <span>- {coin.symbol.toUpperCase()}</span>
          <a href={`https://www.coingecko.com/fr/pi%C3%A8ces/${coin.name.toLowerCase().trim().split(' ').join('-')}`} target="_blank">
            <img src="assets/info-icon.svg" alt="logo question" />
          </a>
        </div>
      </div>
      <p>{ priceFormater(coin.current_price).toLocaleString()} $</p>
      <p className="mktcap">{ mktcapFormater(coin.market_cap).toLocaleString()} M$</p>
      <p className="volume">{ coin.total_volume.toLocaleString()} $</p>
    </div>
  );
};

export default TableLine;