import axios from 'axios';
import React, { useEffect, useState } from 'react';
import PercentChange from './PercentChange';
import TableFilters from './TableFilters';

const HeaderInfos = () => {
  
  const [headerData, setHeaderData] = useState([])
  const [btcData, setBtcData]       = useState(0)
  const [ethData, setEthData]       = useState(0)

  useEffect(()=>{
    axios.get(`https://api.coingecko.com/api/v3/global`)
      .then(resp => {
        setHeaderData(resp.data.data)
        setBtcData(resp.data.data.market_cap_percentage.btc.toFixed(1))
        setEthData(resp.data.data.market_cap_percentage.eth.toFixed(1))
      })
  }, [])
  
  return (
    <div className="header-container">
      <ul className="title">
        <li>
          <h1>
            <img src="assets/logo.png" alt="logo watch tower" />
            Watch Tower
          </h1>
        </li>
        <li>
          Crypto-monnaies : {headerData.active_cryptocurrencies && headerData.active_cryptocurrencies.toLocaleString()}
        </li>
        <li>
          Marchés : {headerData.markets && headerData.markets.toLocaleString()}
        </li>
      </ul>
      <ul className="infos-mkt">
        <li className="global-mkt">
          Global Market Cap :
          <PercentChange percent={headerData.market_cap_change_percentage_24h_usd} />
        </li>
        <li>BTC dominance: {btcData}%</li>
        <li>ETH dominance: {ethData}%</li>
      </ul>
      <TableFilters/>
    </div>
  );
};

export default HeaderInfos;